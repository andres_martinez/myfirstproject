﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPruebas
{
    public class Element
    {
        private int ph;
        private string np;
        private string cn;
        private string a;
        private string t;
        private string c;

        public int Ph { get => ph; set => ph = value; }
        public string Np { get => np; set => np = value; }
        public string Cn { get => cn; set => cn = value; }
        public string A { get => a; set => a = value; }
        public string T { get => t; set => t = value; }
        public string C { get => c; set => c = value; }
    }

    public class Weighing
    {
        private int ph;
        private int np;
        private int cn;
        private int a;
        private int t;
        private int c;

        public int Ph { get => ph; set => ph = value; }
        public int Np { get => np; set => np = value; }
        public int Cn { get => cn; set => cn = value; }
        public int A { get => a; set => a = value; }
        public int T { get => t; set => t = value; }
        public int C { get => c; set => c = value; }
    }
}
