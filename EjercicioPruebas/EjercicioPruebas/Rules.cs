﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPruebas
{
    public class Rules
    {
        //COMPLEJIDA CICLOMATICA 2^5 = 32
        public string getTags(double value)
        {            
            string result = string.Empty;
            if (value < 0)
            {
                result = "ERROR AL ETIQUETAR ELEMENTO";
            }
            else if (value <= 3.8)
            {
                result = "VERDE";
            }
            else if (value >= 3.81 && value <= 8.25)
            {
                result = "AZUL";
            }
            else if (value >= 8.26 && value <= 15)
            {
                result = "AMARILLO";
            }
            else if (value > 15)
            {
                result = "ROJO";
            }
            return result;
        }


        public int getNP(string nivP)
        {
            int result = 0;

            if (nivP.Contains("-"))
            {
                string[] niP = nivP.Split('-');
                foreach (var n in niP)
                {
                    if (int.Parse(n) >= 10 && int.Parse(n) <= 20)
                    {
                        result = 8;
                    }
                    else if (int.Parse(n) >= 20 && int.Parse(n) >= 30)
                    {
                        result = 6;
                    }
                }
            }

            return result;
        }

        //COMPLEJIDA CICLOMATICA 2^3 = 8
        public int getCON(string con)
        {
            int result = 0;
            if (con.Equals("ALTA"))
            {
                result = 20;
            }
            else if (con.Equals("BAJA"))
            {
                result = 10;
            }
            else if (con.Equals("MEDIA"))
            {
                result = 15;
            }

            return result;
        }

        //COMPLEJIDA CICLOMATICA 2^2 = 4
        public int getAC(string ac)
        {
            int result = 0;
            if (ac.Equals("BASICO"))
            {
                result = 2;
            }
            else if (ac.Equals("ALCALINO"))
            {
                result = 4;
            }

            return result;
        }

        //COMPLEJIDA CICLOMATICA 2^2 = 4
        public int getTI(string ti)
        {
            int result = 0;
            if (ti.Equals("MTP"))
            {
                result = 5;
            }
            else if (ti.Equals("HOMEOPATICO"))
            {
                result = 3;
            }

            return result;
        }

        //COMPLEJIDA CICLOMATICA 2^2 = 4
        public int getCOMP(string comp)
        {
            int result = 0;
            if (comp.Equals("ACTIVO"))
            {
                result = 2;
            }
            else if (comp.Equals("EXCIPIENTE"))
            {
                result = 4;
            }

            return result;
        }

        //COMPLEJIDA CICLOMATICA 2^1 = 2
        public Weighing setWeighing(Element element)
        {            
            Weighing w = new Weighing();
            w.Ph = (element.Ph < 0 ? 0 : element.Ph);

            w.Np = getNP(element.Np);
            w.Cn = getCON(element.Cn);
            w.A = getAC(element.A);
            w.T = getTI(element.T);
            w.C = getCOMP(element.C);

            return w;
        }

        //COMPLEJIDA CICLOMATICA 2^0
        public double getTX(Weighing w)
        {
            double f1 = (w.Ph + w.Np) + (w.Cn * w.A);
            double f2 = w.T + w.A;
            double f3 = f1 / f2;

            double tx = double.Parse(Math.Round(f3,2).ToString());

            return tx;
        }
    }
}
