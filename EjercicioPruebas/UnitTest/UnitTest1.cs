﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EjercicioPruebas;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        Rules rules = new Rules();
        Element element = new Element();        

        [TestMethod]
        public void Rules()
        {
            //CRITERIOS DE ACEPTACIÓN HU1
            Assert.AreEqual("ERROR AL ETIQUETAR ELEMENTO", rules.getTags(-1));
            Assert.AreEqual("VERDE", rules.getTags(1));
            Assert.AreEqual("VERDE", rules.getTags(3.8));
            Assert.AreEqual("AZUL", rules.getTags(3.81));
            Assert.AreEqual("AZUL", rules.getTags(8.25));
            Assert.AreEqual("AMARILLO", rules.getTags(8.26));
            Assert.AreEqual("AMARILLO", rules.getTags(15));
            Assert.AreEqual("ROJO", rules.getTags(16));
            Assert.AreEqual("ROJO", rules.getTags(50));
        }

        [TestMethod]
        public void getTx()
        {
            //CRITERIOS DE ACEPTACIÓN HU2
            element.Ph = 7;
            element.Np = "20-30";
            element.Cn = "ALTA";
            element.A = "BASICO";
            element.T = "MTP";
            element.C = "ACTIVO";

            Assert.AreEqual("AZUL", rules.getTags(rules.getTX(rules.setWeighing(element))));
            //Assert.AreEqual("VERDE", rules.getTags(rules.getTX(rules.setWeighing(element))));
            //Assert.AreEqual("AMARILLO", rules.getTags(rules.getTX(rules.setWeighing(element))));
            //Assert.AreEqual("ROJO", rules.getTags(rules.getTX(rules.setWeighing(element))));
        }

        [TestMethod]
        public void getNp()
        {
            //CRITERIOS DE ACEPTACIÓN HU2

            Assert.AreEqual(6, rules.getNP("20-30"));
            Assert.AreEqual(8, rules.getNP("10-20"));
        }

        [TestMethod]
        public void getCon()
        {
            //CRITERIOS DE ACEPTACIÓN HU2

            Assert.AreEqual(20, rules.getCON("ALTA"));
            Assert.AreEqual(15, rules.getCON("MEDIA"));
            Assert.AreEqual(10, rules.getCON("BAJA"));
        }

        [TestMethod]
        public void getAc()
        {
            //CRITERIOS DE ACEPTACIÓN HU2

            Assert.AreEqual(2, rules.getAC("BASICO"));
            Assert.AreEqual(4, rules.getAC("ALCALINO"));
        }

        [TestMethod]
        public void getTi()
        {
            //CRITERIOS DE ACEPTACIÓN HU2

            Assert.AreEqual(5, rules.getTI("MTP"));
            Assert.AreEqual(3, rules.getTI("HOMEOPATICO"));
        }

        [TestMethod]
        public void getComp()
        {
            //CRITERIOS DE ACEPTACIÓN HU2

            Assert.AreEqual(2, rules.getCOMP("ACTIVO"));
            Assert.AreEqual(4, rules.getCOMP("EXCIPIENTE"));
        }
    }
}
